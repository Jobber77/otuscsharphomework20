﻿using Homework20.Visitors;

namespace Homework20.Serializers
{
    public interface ISerializationContext : IShapeVisitor<string>
    {
        ISerializer XmlSerializer { get; }
        ISerializer JsonSerializer { get; }
        void SetSerializer(ISerializer serializer);
    }
}
