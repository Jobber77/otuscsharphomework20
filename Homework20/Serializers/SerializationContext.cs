﻿using Homework20.Visitors;
using System;
using Rectangle = Homework20.Visitors.Rectangle;

namespace Homework20.Serializers
{
    public class SerializationContext : ISerializationContext
    {
        public ISerializer XmlSerializer { get; }
        public ISerializer JsonSerializer { get; }

        private ISerializer _serializer;

        public SerializationContext()
        {
            XmlSerializer = new SimpleXmlSerializer();
            JsonSerializer = new SimpleJsonSerializer();
        }

        public string Visit(Dot shape) => _serializer.Serialize(shape);

        public string Visit(Circle shape) => _serializer.Serialize(shape);

        public string Visit(Rectangle shape) => _serializer.Serialize(shape);

        public void SetSerializer(ISerializer serializer) =>
            _serializer = serializer ?? throw new ArgumentNullException(nameof(serializer));
    }
}
