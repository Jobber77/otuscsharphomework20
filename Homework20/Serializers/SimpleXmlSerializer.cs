﻿using Homework20.Visitors;
using System;

namespace Homework20.Serializers
{
    public class SimpleXmlSerializer : ISerializer
    {
        public string Serialize(Dot shape) =>
            $"<?xml version=\"1.0\" encoding=\"utf-16\"?>{Environment.NewLine}" +
            $"<Dot xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">{Environment.NewLine}" +
            $"\t<X>{shape.X}</X>{Environment.NewLine}" +
            $"\t<Y>{shape.Y}</Y>{Environment.NewLine}" +
            $"</Dot>{Environment.NewLine}";

        public string Serialize(Circle shape) =>
            $"<?xml version=\"1.0\" encoding=\"utf-16\"?>{Environment.NewLine}" +
            $"<Circle xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">{Environment.NewLine}" +
            $"\t<Center>{Environment.NewLine}" +
            $"\t\t<X>{shape.Center.X}</X>{Environment.NewLine}" +
            $"\t\t<Y>{shape.Center.Y}</Y>{Environment.NewLine}" +
            $"\t</Center>{Environment.NewLine}" +
            $"\t<Radius>{shape.Radius}</Radius>{Environment.NewLine}" +
            $"</Circle>";

        public string Serialize(Rectangle shape) =>
            $"<?xml version=\"1.0\" encoding=\"utf-16\"?>{Environment.NewLine}" +
            $"<Rectangle xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">{Environment.NewLine}" +
            $"\t<TopLeftCorner>{Environment.NewLine}" +
            $"\t\t<X>{shape.TopLeftCorner.X}</X>{Environment.NewLine}" +
            $"\t\t<Y>{shape.TopLeftCorner.Y}</Y>{Environment.NewLine}" +
            $"\t</TopLeftCorner>{Environment.NewLine}" +
            $"\t<Width>{shape.Width}</Width>{Environment.NewLine}" +
            $"\t<Height>{shape.Height}</Height>{Environment.NewLine}" +
            $"</Rectangle>";
    }
}
