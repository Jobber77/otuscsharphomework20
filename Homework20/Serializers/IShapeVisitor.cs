﻿using Homework20.Visitors;

namespace Homework20.Serializers
{
    public interface IShapeVisitor<T>
    {
        T Visit(Dot shape);
        T Visit(Circle shape);
        T Visit(Rectangle shape);
    }
}
