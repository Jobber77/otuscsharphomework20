﻿using Homework20.Visitors;

namespace Homework20.Serializers
{
    public interface ISerializer
    {
        string Serialize(Dot shape);
        string Serialize(Circle shape);
        string Serialize(Rectangle shape);
    }
}
