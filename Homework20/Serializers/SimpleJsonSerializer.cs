﻿using Homework20.Visitors;

namespace Homework20.Serializers
{
    public class SimpleJsonSerializer : ISerializer
    {
        public string Serialize(Dot shape) =>
            $"{{\"X\":{shape.X},\"Y\":{shape.Y}}}";

        public string Serialize(Circle shape) =>
            $"{{\"Center\":{{\"X\":{shape.Center.X},\"Y\":{shape.Center.Y}}},\"Radius\":{shape.Radius}}}";

        public string Serialize(Rectangle shape) =>
            $"{{\"TopLeftCorner\":{{\"X\":{shape.TopLeftCorner.X},\"Y\":{shape.TopLeftCorner.Y}}},\"Width\":{shape.Width},\"Height\":{shape.Height}}}";
    }
}
