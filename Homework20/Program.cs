﻿using Homework20.Serializers;
using Homework20.Visitors;
using System;
using System.Collections.Generic;

namespace Homework20
{
    class Program
    {
        static void Main(string[] args)
        {
            var serializationContext = new SerializationContext();
            serializationContext.SetSerializer(serializationContext.JsonSerializer);
            var shapes = new List<IShape>()
            {
                new Dot(10, 10),
                new Circle(new Dot(20, 20), 10),
                new Rectangle(new Dot(30, 30), 10, 10)
            };
            var serializedShapes = new List<string>();
            foreach (var shape in shapes)
                serializedShapes.Add(shape.Accept(serializationContext));
            Console.WriteLine("Serialized shapes:");
            foreach (var shape in serializedShapes)
                Console.WriteLine(shape);
            Console.ReadLine();
        }
    }
}
