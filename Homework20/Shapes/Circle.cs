﻿using Homework20.Serializers;

namespace Homework20.Visitors
{
    public class Circle : IShape
    {
        public Circle() {}
        public Circle(Dot center, int radius)
        {
            Center = center;
            Radius = radius;
        }
        public Dot Center { get; set; }
        public int Radius { get; set; }
        public T Accept<T>(IShapeVisitor<T> shapeVisitor) => shapeVisitor.Visit(this);
    }
}
