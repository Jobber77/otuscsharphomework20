﻿using Homework20.Serializers;

namespace Homework20.Visitors
{
    public interface IShape
    {
        T Accept<T>(IShapeVisitor<T> shapeVisitor);
    }
}
