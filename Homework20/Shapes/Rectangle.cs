﻿using Homework20.Serializers;

namespace Homework20.Visitors
{
    public class Rectangle : IShape
    {
        public Rectangle() {}
        public Rectangle(Dot topLeftCorner, int width, int height)
        {
            TopLeftCorner = topLeftCorner;
            Width = width;
            Height = height;
        }
        public Dot TopLeftCorner { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public T Accept<T>(IShapeVisitor<T> shapeVisitor) => shapeVisitor.Visit(this);
    }
}
