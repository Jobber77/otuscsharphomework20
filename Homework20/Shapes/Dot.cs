﻿using Homework20.Serializers;

namespace Homework20.Visitors
{
    public class Dot : IShape
    {
        public Dot() {}
        public Dot(int x, int y)
        {
            X = x;
            Y = y;
        }

        public int X { get; set; }
        public int Y { get; set; }
        public T Accept<T>(IShapeVisitor<T> shapeVisitor) => shapeVisitor.Visit(this);
    }
}
